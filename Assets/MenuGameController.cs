﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuGameController : MonoBehaviour
{
    [Tooltip("_sceneToLoadOnPlay is the name of the scene that will be loaded when users click play")]
    public string _sceneToLoadOnPlay = "MainScene";

    public GameObject MainMenu;

    [Tooltip("_Creditos")]
    public GameObject Creditos;

    //The private variable 'scene' defined below is used for example/development purposes.
    //It is used in correlation with the Escape_Menu script to return to last scene on key press.
    UnityEngine.SceneManagement.Scene scene;

    void Awake()
    {
        scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
        PlayerPrefs.SetString("_LastScene", scene.name.ToString());
        Debug.Log(scene.name);        
    }

    public void PlayGame(int numberOfPlayers)
    {
        GameState.numberOfPlayers = numberOfPlayers;
        GameState.Reset();

        Debug.Log($"Cena:" + _sceneToLoadOnPlay);
        PlayerPrefs.SetString("_LastScene", scene.name);
        UnityEngine.SceneManagement.SceneManager.LoadScene(_sceneToLoadOnPlay);
    }

    public void ShowCreditos()
    {
        var texto = Creditos.GetComponent<Text>();

        MainMenu.SetActive(false);
        Creditos.SetActive(true);
        StartCoroutine(Sleap());        
    }


    IEnumerator Sleap()
    {
        yield return new WaitForSeconds(19);
        Creditos.SetActive(false);
        MainMenu.SetActive(true);
    }

    public void QuitGame()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif

#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#endif
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
