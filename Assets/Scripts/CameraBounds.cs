﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBounds : MonoBehaviour
{
    public Camera cam;
    public Plane plane;
    public float upperDistance = 10.0f;
    public float lowerDistance = 5.0f;

    private Transform _transform;

    // Start is called before the first frame update
    void Start()
    {
        _transform = cam.transform;
        FindUpperCorners();
        FindLowerCorners();
        test();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void test()
    {
        var upper = GetCorners(upperDistance);
        var lower = GetCorners(lowerDistance);

        Debug.DrawLine(upper[0], lower[0], Color.cyan, 1000);
        Debug.DrawLine(upper[1], lower[1], Color.cyan, 1000);
        Debug.DrawLine(upper[2], lower[2], Color.cyan, 1000);
        Debug.DrawLine(upper[3], lower[3], Color.cyan, 1000);

        var r1 = Vector3.Lerp(upper[1], upper[3], 0.5f);
        var r2 = Vector3.Lerp(lower[1], lower[3], 0.5f);
        Debug.DrawLine(r1, r2, Color.magenta, 1000);

        var l1 = Vector3.Lerp(upper[2], upper[0], 0.5f);
        var l2 = Vector3.Lerp(lower[2], lower[0], 0.5f);
        Debug.DrawLine(l1, l2, Color.magenta, 1000);

        var mr = Vector3.Lerp(r1, r2, 0.5f);
        var ml = Vector3.Lerp(l1, l2, 0.5f);

        var objR = GameObject.CreatePrimitive(PrimitiveType.Cube);

        var objL = GameObject.CreatePrimitive(PrimitiveType.Cube);


        Instantiate(objR, mr, Quaternion.FromToRotation(r1, r2));
        Instantiate(objL, ml, Quaternion.FromToRotation(l1, l2));

    }

    private void FindUpperCorners()
    {
        var corners = GetCorners(upperDistance);

        // for debugging
        Debug.DrawLine(corners[0], corners[1], Color.yellow, 1000); // UpperLeft -> UpperRight
        Debug.DrawLine(corners[1], corners[3], Color.yellow, 1000); // UpperRight -> LowerRight
        Debug.DrawLine(corners[3], corners[2], Color.yellow, 1000); // LowerRight -> LowerLeft
        Debug.DrawLine(corners[2], corners[0], Color.yellow, 1000); // LowerLeft -> UpperLeft

        //CreatePlane(corners[0], corners[1], corners[3], corners[2]);
    }


    private void FindLowerCorners()
    {
        var corners = GetCorners(lowerDistance);

        // for debugging
        Debug.DrawLine(corners[0], corners[1], Color.red, 1000);
        Debug.DrawLine(corners[1], corners[3], Color.red, 1000);
        Debug.DrawLine(corners[3], corners[2], Color.red, 1000);
        Debug.DrawLine(corners[2], corners[0], Color.red, 1000);


        //CreatePlane(corners[0], corners[1], corners[3], corners[2]);
    }

    private Vector3[] GetCorners(float distance)
    {
        var corners = new Vector3[4];

        float halfFOV = (cam.fieldOfView * 0.5f) * Mathf.Deg2Rad;
        float aspect = cam.aspect;

        float height = distance * Mathf.Tan(halfFOV);
        float width = height * aspect;

        // UpperLeft
        corners[0] = _transform.position - (_transform.right * width);
        corners[0] += _transform.up * height;
        corners[0] += _transform.forward * distance;

        // UpperRight
        corners[1] = _transform.position + (_transform.right * width);
        corners[1] += _transform.up * height;
        corners[1] += _transform.forward * distance;

        // LowerLeft
        corners[2] = _transform.position - (_transform.right * width);
        corners[2] -= _transform.up * height;
        corners[2] += _transform.forward * distance;

        // LowerRight
        corners[3] = _transform.position + (_transform.right * width);
        corners[3] -= _transform.up * height;
        corners[3] += _transform.forward * distance;

        return corners;
    }

    private void CreatePlane(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4 )
    {
        Mesh mesh = new Mesh();

        var uvs = new[]
        {
            new Vector2(0.0f,1.0f),
            new Vector2(1.0f,1.0f),
            new Vector2(0.0f,0.0f),
            new Vector2(1.0f,0.0f)
        };
        var tris = new[] { 0, 1, 2, 2, 1, 3 };

        mesh.vertices = new[] { v1, v2, v3, v4 };
        mesh.triangles = tris;
        mesh.uv = uvs;

        mesh.RecalculateNormals();

        var obj = new GameObject();
        obj.AddComponent<MeshFilter>().mesh = mesh;
        obj.AddComponent<MeshRenderer>();

        Instantiate(obj);
    }
}
