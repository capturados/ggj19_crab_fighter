﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public AudioSource WinAudio;
    public AudioSource GameAudio;

    // Start is called before the first frame update
    void Start()
    {
        GameState.gameEvent.AddListener(OnGameEvent);        
    }

    private void OnDestroy()
    {
        GameState.gameEvent.RemoveAllListeners();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("RemainingTimeRatio: " + GameState.RemainingTimeRatio);
        GameState.Update();
    }

    void OnGameEvent(string eventName, int playerIndex)
    {
        switch(eventName)
        {
            case "startFight":
                {
                    Debug.Log("Start Fight!");
                    break;
                }
            case "finish":
                {
                    Debug.Log("Finish!");

                    GameAudio.Stop();
                    WinAudio.Play();
                    break;
                }
            case "quit":
                {
                    Debug.Log("Quit!");
                    break;
                }
        }
    }
}
