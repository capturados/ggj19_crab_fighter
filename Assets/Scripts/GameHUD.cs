﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameHUD : MonoBehaviour
{
    public GameObject Fight;
    public GameObject Finish;

    public Text Timer;
    public Text PlacarPlayer1;
    public Text PlacarPlayer2;
    public Text PlacarPlayer3;
    public Text PlacarPlayer4;

    // Start is called before the first frame update
    void Start()
    {
        GameState.gameEvent.AddListener(OnGameEvent);
    }

    // Update is called once per frame
    void Update()
    {
        float tempo = GameState.RemainingTime;
        Timer.text = string.Format("{0:00}:{1:00}", (int) tempo / 60, (int) tempo % 60);

        for (int i = 1; i <= GameState.numberOfPlayers; i++)
        {            
            switch (i)
            {                
                case 1:                    
                    PlacarPlayer1.text = GameState.player[i].score.ToString();
                    break;
                case 2:                    
                    PlacarPlayer2.text = GameState.player[i].score.ToString();
                    break;
                case 3:                   
                    PlacarPlayer3.text = GameState.player[i].score.ToString();
                    break;
                case 4:                    
                    PlacarPlayer4.text = GameState.player[i].score.ToString();
                    break;
            }            
        }
    }

    void OnGameEvent(string eventName, int playerIndex)
    {
        switch (eventName)
        {
            case "startFight":
                {
                    Fight.SetActive(false);                    
                    Timer.gameObject.SetActive(true);
                    break;
                }
            case "finish":
                {
                    Finish.SetActive(true);
                    Timer.gameObject.SetActive(false);

                    break;
                }
            case "quit":
                {
                    PlayerPrefs.SetString("_LastScene", "MainScene");
                    ScenesParams.ScenesStore.setParam("player", playerIndex.ToString());
                    ScenesParams.ScenesStore.setParam("points", GameState.player[playerIndex].score.ToString());
                    SceneManager.LoadScene("RankingMenu");


                    break;
                }
        }
    }
}
