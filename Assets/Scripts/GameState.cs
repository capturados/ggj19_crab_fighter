﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class PlayerEvent : UnityEvent<int, string, int> //PlayerOrigin, EventName, PlayerDestiny
{

}

[System.Serializable]
public class GameEvent : UnityEvent<string, int> //EventName, PlayerOrigin
{

}

public static class GameState
{
    public const int MIN_PLAYERS = 2;
    public const int MAX_PLAYERS = 4;

    public const int INITIAL_SCORE = 100;
    public const int BASE_ATTACK_HIT = 20;
    public const int BASE_SHELL_DEFENCE = 30;

    public const float SECONDS_TO_START_FIGHT = 1.5f;

    public const float SECONDS_ATTACKING = 0.5f;
    public const float SECONDS_TO_APPLY_ATTACK = SECONDS_ATTACKING / 2.0f;

    public static float SECONDS_TO_FINISH = 60f;
    public const float SECONDS_TO_QUIT = 5.0f;

    private static int _numberOfPlayers = MAX_PLAYERS;
    public static int numberOfPlayers {
        get
        {
            return _numberOfPlayers;
        }
        set
        {
            _numberOfPlayers = value;
            switch(value)
            {
                case 2:
                    SECONDS_TO_FINISH = 40f + 0.49f;
                    break;
                case 3:
                    SECONDS_TO_FINISH = 60f + 0.49f;
                    break;
                case 4:
                    SECONDS_TO_FINISH = 90f + 0.49f;
                    break;
            }
        }
    }

    private static Color[] playerColors = new Color[4] { new Color(214f/255f,8f/255f,  255f/255f,1.0f), //roxo
                                                         new Color(255f/255f,120f/255f,40f/255f,1.0f), //laranja
                                                         new Color(0f/255f, 149f/255f, 39f/255f,1.0f), //verde
                                                         new Color(1f/255f, 134f/255f, 255f/255f,1.0f)};//azul

    public static GameEvent gameEvent = new GameEvent();

    static GameState()
    {
        Reset();
    }

    
    private static bool isPlaying;
    private static float timePlaying;
    private static float timeFinished;
    public static bool IsPlaying {
        get
        {
            return isPlaying;
        }
        set {
            timePlaying = 0.0f;
            timeFinished = 0.0f;
            isPlaying = value;
        }
    }

    public static bool IsFinished { get; set; }

    public static float ElapsedTime {
        get
        {
            return timePlaying;
        }
    }

    public static float RemainingTime
    {
        get
        {
            return SECONDS_TO_FINISH - timePlaying;
        }
    }

    public static float ElapsedTimeRatio
    {
        get
        {
            return timePlaying / SECONDS_TO_FINISH;
        }
    }

    public static float RemainingTimeRatio
    {
        get
        {
            return RemainingTime / SECONDS_TO_FINISH;
        }
    }

    public static void Pause()
    {
        isPlaying = false;
    }

    public static void Resume()
    {
        isPlaying = true;
    }

    public static void TogglePauseResume()
    {
        isPlaying = true;
    }

    private static bool isFightStarted;
    public static bool IsFightStarted
    {
        get
        {
            return isFightStarted;
        }
        set
        {
            isFightStarted = value;
        }
    }

    public static int CurrentWinner
    {
        get
        {
            var maxScore = 0;
            var playerIndex = 0;
            for(var i = 1; i <= numberOfPlayers; i++)
            {
                if(player[i].score > maxScore)
                {
                    maxScore = player[i].score;
                    playerIndex = i;
                }
            }
            return playerIndex;
        }
    }

    public static bool Tie //(Empate)
    {
        get
        {
            var maxScore = 0;
            var playerIndex = 0;
            for (var i = 1; i <= numberOfPlayers; i++)
            {
                if (player[i].score > maxScore)
                {
                    maxScore = player[i].score;
                    playerIndex = i;
                }
            }

            for (var i = 1; i <= numberOfPlayers; i++)
            {
                if (i != playerIndex && player[i].score == maxScore)
                {
                    return true;
                }
            }
            return false;
        }
    }

    public class PlayerData
    {
        public int score;

        public bool hasShell;
        public int shellDamage;

        public bool attacking;
        public List<int> victims;
        public bool apliedAttack;

        //public bool invulnerable;
        public bool died;

        public float timeToAttackAgain;

        public PlayerEvent playerEvent;

        public Color color;

        //public float timeToRevive;
        //public float timeToLoseInvulnerability;
    }

    public static PlayerData[] player; //Não usar player[0]!

    public static void Reset()
    {
        player = new PlayerData[MAX_PLAYERS+1];
        for(int i = 1; i <= numberOfPlayers; i++)
        {
            player[i] = new PlayerData();
            player[i].score = INITIAL_SCORE;
            player[i].victims = new List<int>();
            player[i].playerEvent = new PlayerEvent();
            player[i].color = playerColors[i-1];
        }
        IsPlaying = true;
        IsFinished = false;
        isFightStarted = false;
    }


    public static void PickShell(int playerIndex)
    {
        player[playerIndex].hasShell = true;
        player[playerIndex].shellDamage = 0;
    }

    public static bool CanAttack(int attackerIndex)
    {
        return IsPlaying && IsFightStarted && player[attackerIndex].attacking == false;
    }

    public static void StartAttack(int attackerIndex)
    {
        var attacker = player[attackerIndex];
        attacker.attacking = true;
        attacker.apliedAttack = false;
        attacker.timeToAttackAgain = SECONDS_ATTACKING;
    }

    public static bool ApplyAttack(int attackerIndex, int victimIndex)
    {
        var attacker = player[attackerIndex];
        var victim = player[victimIndex];

        //Debug.Log("Victim: " + victimIndex + " from " + attackerIndex);
        if(victim.died)
        {
            return false;
        }

        if(victim.hasShell)
        {
            victim.shellDamage += BASE_ATTACK_HIT;
            victim.hasShell = victim.shellDamage < BASE_SHELL_DEFENCE;
        }
        else
        {
            if (victim.score > 0)
            {
                var availScore = Mathf.Min(victim.score, Mathf.RoundToInt( Mathf.Max( BASE_ATTACK_HIT, victim.score * (BASE_ATTACK_HIT / 100.0f) ) ) );
                attacker.score+= availScore;
                victim.score-= availScore;
            }
            victim.died = victim.score == 0;
            if(victim.died)
            {
                CheckFinishingByLastSurvivor();
            }
        }

        attacker.playerEvent.Invoke(attackerIndex, "applyAttackerOn", victimIndex);
        victim.playerEvent.Invoke(victimIndex, "receiveDamageFrom", attackerIndex);

        return true;
    }

    public static int TotalAlive {
        get
        {
            var alive = 0;
            for (var i = 1; i <= numberOfPlayers; i++)
            {
                if (player[i].died == false)
                {
                    alive++;
                }
            }
            return alive;
        }
    }

    public static void CheckFinishingByLastSurvivor()
    {
        if(TotalAlive <= 1)
        {
            IsFinished = true;
            gameEvent.Invoke("finish", CurrentWinner);
        }
    }

    public static void Update()
    {
        if(IsPlaying)
        {
            timePlaying += Time.deltaTime;
            //Debug.Log("timePlaying: " + timePlaying + "/" + SECONDS_TO_START_FIGHT);

            if (timePlaying >= SECONDS_TO_START_FIGHT)
            {
                if(IsFightStarted == false)
                {
                    timePlaying = 0.0f;
                    gameEvent.Invoke("startFight", CurrentWinner);
                    IsFightStarted = true;
                }
            }

            for (int i = 1; i <= numberOfPlayers; i++)
            {
                var p = player[i];
                if(p.attacking)
                {
                    
                    p.timeToAttackAgain -= Time.deltaTime;
                    if(p.timeToAttackAgain < 0.0)
                    {
                        p.attacking = false;
                    }
                    if(p.apliedAttack == false)
                    {
                        var elapsedTimeSinceAttacked = SECONDS_ATTACKING - p.timeToAttackAgain;
                        if (elapsedTimeSinceAttacked > SECONDS_TO_APPLY_ATTACK)
                        {
                            foreach (var victimIndex in p.victims)
                            {
                                ApplyAttack(i, victimIndex);
                            }
                            p.apliedAttack = true;
                        }
                    }
                }
            }

            if(IsFinished)
            {
                timeFinished += Time.deltaTime;
                if(timeFinished > SECONDS_TO_QUIT)
                {
                    IsPlaying = false;
                    IsFightStarted = false;
                    gameEvent.Invoke("quit", CurrentWinner);
                }
            }

            if (timePlaying >= SECONDS_TO_FINISH && Tie == false)
            {
                IsFinished = true;
                
                gameEvent.Invoke("finish", CurrentWinner);
            }
        }
    }
    
}
