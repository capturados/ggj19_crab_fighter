﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [HideInInspector]
    public bool facingRight = true;         // For determining which way the player is currently facing.

    public ParticleSystem pickShellEffect;
    public ParticleSystem hitEffect;

    public float hitImpact = 1;
    public float hitRecoveryTime = 2;

    public float moveForce = 20f;          // Amount of force added to move the player left and right.

    private Animator animator;                  // Reference to the player's animator component.

    public int playerNum;

    private float dieForce = 0.25f;
    
    public float knockBackForce = 5.5f;
    public float knockBackDuration = 0.45f;
    private bool knockBackActive = false;
    private float knockBackRemainingTime;
    private float knockbackSignal;

    public List<AudioClip> hitSounds;

    [HideInInspector]
    public bool isRecovering;
    private float recoveryTimer;

    void StartKnockBack(float signal)
    {
        knockbackSignal = signal;
        knockBackRemainingTime = knockBackDuration;
        knockBackActive = true;
    }
    
    private float pickShellStopTime = 0.65f;
    private float pickShellCurrentStopTime = 0f;
    private bool hasGotShell;

    [Tooltip("Container onde fica a concha do player")]
    public GameObject shellContainer;

    [HideInInspector]
    public bool hasShell
    {
        get
        {
            return GameState.player[playerNum].hasShell;
        }
    }

    private string verticalKey
    {
        get => "Vertical" + playerNum;
    }
    private string horizontalKey
    {
        get => "Horizontal" + playerNum;
    }
    private string attackKey
    {
        get => "Attack" + playerNum;
    }

    private int altNum
    {
        get
        {
            if(GameState.numberOfPlayers == 2)
            {
                if (playerNum == 1) return 3;
                else return 4;
            }
            else if(GameState.numberOfPlayers == 3)
            {
                if (playerNum == 1) return 4;
                else return playerNum;
            }
            else
            {
                return playerNum;
            }
        }
    }

    private string verticalKeyAlt
    {
        get => "Vertical" + altNum;
    }
    private string horizontalKeyAlt
    {
        get => "Horizontal" + altNum;
    }
    private string attackKeyAlt
    {
        get => "Attack" + altNum;
    }

    void Awake()
    {



        // Setting up references.
        animator = GetComponent<Animator>();

        if (playerNum > GameState.numberOfPlayers)
        {
            Destroy(gameObject);
        }
        else
        {

            // Setting up effects.
            hitEffect.scalingMode = ParticleSystemScalingMode.Local;
            hitEffect.transform.localScale = new Vector3(1, 1, 1) * 0.4f * hitImpact;

            pickShellEffect.scalingMode = ParticleSystemScalingMode.Local;
            pickShellEffect.transform.localScale = new Vector3(1, 1, 0.2f) * 0.4f;

            animator = GetComponent<Animator>();

            //valida se possui a seta e define sua cor
            foreach (var setaSpriteRenderer in GetComponentsInChildren<SpriteRenderer>(true))
            {
                if (setaSpriteRenderer.CompareTag("playerSeta"))
                {
                    Debug.Log("Player num: " + playerNum);
                    setaSpriteRenderer.color = GameState.player[playerNum].color;
                }
            }

            //Importante colocar o listener
            GameState.player[playerNum].playerEvent.AddListener(OnPlayerEvent);
        }

    }

    private void Start()
    {

    }

    private PlayerController FindPlayerByIndex(int index)
    {
        var players = Object.FindObjectsOfType<PlayerController>();
        foreach (var player in players)
        {
            if (player.playerNum == index)
            {
                return player;
            }
        }
        return null;
    }

    private void OnDestroy()
    {
        if (GameState.player[playerNum] != null)
        {
            GameState.player[playerNum].playerEvent.RemoveAllListeners();
        }
    }

    void OnPlayerEvent(int playerOriginIndex, string eventName, int playerDestinyIndex)
    {
        switch (eventName)
        {
            case "receiveDamageFrom":
                {
                    if (!isRecovering) {
                        isRecovering = true;
                        ShowHitEffect();
                        var attacker = FindPlayerByIndex(playerDestinyIndex);
                        var signal = attacker.gameObject.transform.position.x > gameObject.transform.position.x ? -1.0f : 1.0f;
                        //Debug.Log("Player " + playerOriginIndex + " receive dmg from " + playerDestinyIndex);
                        //GetComponent<Rigidbody>().AddForce(new Vector3(knockBackForce * signal, 0.0f, 0.0f));

                        if(!GameState.player[playerOriginIndex].hasShell)
                        {
                            if(animator.GetBool("HasShell"))
                            {
                                animator.SetBool("HasShell", false);
                                shellContainer.transform.position = transform.position + (Vector3.up * 1.2f);
                                var shell = shellContainer.GetComponent<ShellOut>();
                                shell.LaunchShell();
                                shellContainer = null;
                            }
                        }

                        StartKnockBack(signal);
                    }
                    break;
                }
        }
    }

    void ShowPickShellEffect()
    {
        Instantiate(pickShellEffect, transform);
    }

    void ShowHitEffect()
    {
        var pos = transform.position;
        var hitPos = (0.2f * (facingRight ? 1 : -1));
        Instantiate(hitEffect, new Vector3(pos.x + hitPos, pos.y, pos.z), Quaternion.identity, transform);
        if(hitSounds.Count > 0) { 
            var audioClip = hitSounds[Random.Range(0, hitSounds.Count())];
            GetComponent<AudioSource>().PlayOneShot(audioClip);
        }
    }

    void Update()
    {
        if (GameState.IsPlaying == false || GameState.player[playerNum].died || GameState.IsFinished) return;

        //Atacar!
        if ( (Input.GetButtonDown(attackKey) || Input.GetButtonDown(attackKeyAlt)) && GameState.CanAttack(playerNum))
        {
            animator.SetTrigger("Attack");
            GameState.StartAttack(playerNum);
        }
    }


    void FixedUpdate()
    {
        if (isRecovering)
        {
            var rederer  = this.gameObject.GetComponent<SpriteRenderer>();
            rederer.enabled = !rederer.enabled;
            recoveryTimer -= Time.deltaTime;
            if(recoveryTimer <= 0)
            {
                rederer.enabled = true;
                isRecovering = false;
            }
        }
        else
        {
            recoveryTimer = hitRecoveryTime;
        }

        //Knock back
        if (knockBackActive)
        {
            var moveForce = knockBackForce * knockbackSignal;
            knockBackRemainingTime -= Time.fixedDeltaTime;
            if(knockBackRemainingTime < 0)
            {
                knockBackActive = false;
            }
            transform.position += Vector3.right * moveForce * Time.fixedDeltaTime;
            return;
        }

        //Go down when die...
        if(GameState.player[playerNum].died)
        {
            var rigidbody = GetComponent<Rigidbody>();
            if (rigidbody != null)
            {
                Destroy(rigidbody);
            }
            transform.position += Vector3.down * dieForce * Time.fixedDeltaTime;
        }

        //Exit if not playing
        if (GameState.IsPlaying == false || GameState.player[playerNum].died || GameState.IsFinished)
        {
            return;
        }

        var verticalVelocityAbs = System.Math.Abs(Input.GetAxis(verticalKey));
        var horizontalVelocityAbs = System.Math.Abs(Input.GetAxis(horizontalKey));
        var verticalVelocityAbsAlt = System.Math.Abs(Input.GetAxis(verticalKeyAlt));
        var horizontalVelocityAbsAlt = System.Math.Abs(Input.GetAxis(horizontalKeyAlt));
        if (verticalVelocityAbsAlt > verticalVelocityAbs) verticalVelocityAbs = verticalVelocityAbsAlt;
        if (horizontalVelocityAbsAlt > horizontalVelocityAbs) horizontalVelocityAbs = horizontalVelocityAbsAlt;

        float localMoveForce = moveForce;
        if (hasGotShell && pickShellCurrentStopTime <= pickShellStopTime)
        {
            localMoveForce = 0;
            pickShellCurrentStopTime += Time.deltaTime;
            verticalVelocityAbs = 0;
            horizontalVelocityAbs = 0;
        }
        else
        {
            pickShellCurrentStopTime = 0;
            hasGotShell = false;
            if(GameState.player[playerNum].hasShell == false)
            {
                localMoveForce = localMoveForce * 1.15f;
            }
        }
        
        //VERTICAL
        if (Input.GetAxis(verticalKey) > 0f || Input.GetAxis(verticalKeyAlt) > 0f)
        {
            transform.position += Vector3.forward * localMoveForce * Time.fixedDeltaTime;
        }
        else if (Input.GetAxis(verticalKey) < 0f || Input.GetAxis(verticalKeyAlt) < 0f)
        {
            transform.position += Vector3.back * localMoveForce * Time.fixedDeltaTime;
        }

        //HORIZONTAL
        if (knockBackActive == false)
        {

            if (Input.GetAxis(horizontalKey) > 0f || Input.GetAxis(horizontalKeyAlt) > 0f)
            {
                transform.position += Vector3.right * localMoveForce * Time.fixedDeltaTime;
            }
            else if (Input.GetAxis(horizontalKey) < 0f || Input.GetAxis(horizontalKeyAlt) < 0f)
            {
                transform.position += Vector3.left * localMoveForce * Time.fixedDeltaTime;
            }
        }

        var velocityResult = verticalVelocityAbs + horizontalVelocityAbs;
        animator.SetFloat("Speed", velocityResult);

        //// Cache the horizontal & vertical input.
        float h = Input.GetAxis(horizontalKey) + Input.GetAxis(horizontalKeyAlt);

        // If the input is moving the player right and the player is facing left...
        if (h > 0 && !facingRight)
            // ... flip the player.
            Flip();
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (h < 0 && facingRight)
            // ... flip the player.
            Flip();
    }


    void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (enter)
        //{

        var otherPlayer = other.GetComponent<PlayerController>();
        if(otherPlayer != null && otherPlayer.playerNum != playerNum && otherPlayer.isRecovering == false)
        {
            var contains = GameState.player[playerNum].victims.Contains(otherPlayer.playerNum);
            if (contains == false)
            {
                GameState.player[playerNum].victims.Add(otherPlayer.playerNum);
            }

            // Debug.Log(playerNum + " entered in player " + otherPlayer.playerNum + " at " + Time.deltaTime);
        }

        //}
    }

    private void OnTriggerExit(Collider other)
    {
        var otherPlayer = other.GetComponent<PlayerController>();
        if (otherPlayer != null && otherPlayer.playerNum != playerNum)
        {
            while (GameState.player[playerNum].victims.Remove(otherPlayer.playerNum));
            //Debug.Log(playerNum + " exit in player " + otherPlayer.playerNum);
        }
    }

    /*private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(playerNum + " collision in " + collision.ToString());
    }*/

    public GameObject GetPlayerShellContainer()
    {
        return shellContainer;
    }

    public void PickShell(GameObject shell)
    {
        //comunica ao game state
        GameState.PickShell(playerNum);
        ShowPickShellEffect();
        
        //adiciona a concha trocando de sprite
        this.shellContainer = shell;
        animator.SetBool("HasShell", true);
        hasGotShell = true;
    }

}
