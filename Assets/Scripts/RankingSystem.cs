﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RankingSystem : MonoBehaviour
{
    public class RankingStorage
    {
        private static List<RankingData> _playerData;
        public static List<RankingData> PlayerData
        {
            get
            {
                if (_playerData == null)
                {
                    _playerData = new List<RankingData>();
                }
                return _playerData;
            }
            set
            {
                _playerData = value;
            }
        }

        public static void PushToRanking(string player, int points)
        {

            PlayerData.Add(new RankingData() { PlayerName = player, Points = points });
            PlayerData = PlayerData.OrderByDescending(p => p.Points).ToList();

            ////Push into hall
            //for (int i = 0; i < PlayerData.Count; i++)
            //{
            //    if (points > PlayerData[i].Points)
            //    {
            //        //insere na posicao
            //        PlayerData.Insert(i, new RankingData() { PlayerName = player, Points = points });
            //        break;
            //    }
            //}

            if (PlayerData.Count > 5)
            {
                PlayerData.RemoveAt(PlayerData.Count - 1);
            }
        }
    }

    public class RankingData
    {
        public string PlayerName { get; set; }
        public int Points { get; set; }
    }

    public string PlayerWinnerIndex { get; set; }
    public string PlayerWinnerName { get; set; }
    public int PlayerPoints { get; set; }
    public List<GameObject> PlayerDisplayPlacar;
    public GameObject RankingTable;
    public GameObject RankingWhatsNameHolder;
    public GameObject InputGameObject;


    // Start is called before the first frame update
    void Start()
    {
        if (ScenesParams.ScenesStore.getSceneParameters() != null)
        {
            PlayerWinnerIndex = ScenesParams.ScenesStore.getParam("player");
            PlayerPoints = int.Parse(ScenesParams.ScenesStore.getParam("points"));

            //remove
            ScenesParams.ScenesStore.removeParam("player");
            ScenesParams.ScenesStore.removeParam("points");
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < PlayerDisplayPlacar.Count; i++)
        {
            //get text render component
            var textRender = PlayerDisplayPlacar[i].GetComponent<Text>();

            if (RankingStorage.PlayerData.Count > i && RankingStorage.PlayerData[i] != null)
            {
                textRender.text = $"{i + 1} - PLAYER: {RankingStorage.PlayerData[i].PlayerName}    | POINTS: {RankingStorage.PlayerData[i].Points}";
            }
            else
            {
                textRender.text = "";
            }
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter) && !RankingTable.active)
        {
            RegisterPlayer();
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && !RankingTable.active)
        {
            SkipRegister();
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && RankingTable.active)
        {
            ClickBackToPlay();
        }

    }

    public void ClickBackToPlay()
    {
        Debug.Log("Load main Menu");
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    public void RegisterPlayer()
    {
        var inputField = InputGameObject.GetComponent<InputField>();
        PlayerWinnerName = inputField.text;

        if (PlayerWinnerIndex == null)
            PlayerWinnerIndex = "0";

        RankingStorage.PushToRanking(string.IsNullOrWhiteSpace(PlayerWinnerName) ? PlayerWinnerIndex.ToString() : PlayerWinnerName, PlayerPoints);

        RankingWhatsNameHolder.SetActive(false);
        RankingTable.SetActive(true);
    }

    public void SkipRegister()
    {
        RankingWhatsNameHolder.SetActive(false);
        RankingTable.SetActive(true);
    }
}
