﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesParams : MonoBehaviour
{
    public static class ScenesStore
    {

        private static Dictionary<string, string> parameters;

        public static void Load(string sceneName, Dictionary<string, string> parameters = null)
        {
            ScenesStore.parameters = parameters;
            SceneManager.LoadScene(sceneName);
        }

        public static void Load(string sceneName, string paramKey, string paramValue)
        {
            ScenesStore.parameters = new Dictionary<string, string>();
            ScenesStore.parameters.Add(paramKey, paramValue);
            SceneManager.LoadScene(sceneName);
        }

        public static Dictionary<string, string> getSceneParameters()
        {
            return parameters;
        }

        public static string getParam(string paramKey)
        {
            if (parameters == null)
                return "";
            return parameters[paramKey];
        }

        public static void setParam(string paramKey, string paramValue)
        {
            if (parameters == null)
                ScenesStore.parameters = new Dictionary<string, string>();
            ScenesStore.parameters.Add(paramKey, paramValue);
        }

        public static void removeParam(string paramKey)
        {
            if (parameters == null)
                return;

            ScenesStore.parameters.Remove(paramKey);
        }

    }
}
