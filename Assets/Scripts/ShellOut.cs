﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellOut : MonoBehaviour
{
    public float unpickableCooldownTime = 1;
    private bool unpickable = false;
    private float unpickableTimer;

    // Start is called before the first frame update
    void Start()
    {
        unpickableTimer = unpickableCooldownTime;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.identity;
    }

    void FixedUpdate()
    {
        if (unpickable)
        {
            unpickableTimer -= Time.deltaTime;
            if (unpickableTimer <= 0)
            {
                unpickable = false;
            }
        }
        else
        {
            unpickableTimer = unpickableCooldownTime;
        }
    }

    public void LaunchShell()
    {
        this.unpickable = true;
        this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        this.gameObject.GetComponent<BoxCollider>().enabled = true;

        var force = new Vector3(
            0,//(Random.value + 1), 
            10,
            0//(Random.value + 1)
        );

        this.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(), ForceMode.Impulse);
    }

    void OnCollisionEnter(Collision col)
    {
        if (!unpickable)
        {
            if (col.gameObject.name.ToUpper().Contains("PLAYER"))
            {
                var player = col.gameObject.GetComponent<PlayerController>();
                if (player == null)
                {
                    Debug.LogError("player nao encontrado na colisao");
                    return;
                }

                if (!player.hasShell && !player.isRecovering)
                {
                    this.gameObject.GetComponent<BoxCollider>().enabled = false;
                    this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    player.PickShell(this.gameObject);
                }
            }
        }
    }
}
