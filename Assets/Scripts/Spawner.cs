﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject PlacarPlayer3;
    public GameObject PlacarPlayer4;

    public GameObject shellPrefab;

    //private float multiplierToRespawn = 28.0f; //aumente aqui para dificultar a aparicao de conchas
    //private float secondsToRespawnShells = 10.0f; //multiplicador padrao 10 (nao mexer)
    //private float elapsedTimeToRespawnShells = 0.0f;

    //private float initialShells = 0f;
    //private float decayOfShells = 0.40f;
    //private float currentSheels;

    private float maxFar;
    private float minNear;
    private float maxY = 1.0f;
    private float minY = 0.75f;

    private int[] maxTimes = new int[3] { 20, 20, 20 };
    private int[] minTimes = new int[3] { 10, 10, 10 };
    private float timeRemainingToSpawn = 0.0f;
    private int shellsCount = 0;

    void defineNewTimeToSpawn()
    {
        var aditionalDelay = Random.Range(0.0f, 5.0f);

        var gameIndex = GameState.numberOfPlayers - 2;
        var maxTime = maxTimes[gameIndex];
        var minTime = minTimes[gameIndex];

        var currentTime = minTime + (maxTime - minTime) * GameState.ElapsedTimeRatio;

        timeRemainingToSpawn = currentTime + aditionalDelay;
    }

    // Start is called before the first frame update
    void Start()
    {
        maxFar = Camera.main.farClipPlane / 120.0f;
        minNear = Camera.main.nearClipPlane / 0.10f;
        //maxFar = minNear;
        //minNear = maxFar;

        defineNewTimeToSpawn();
        //timeRemainingToSpawn += GameState.SECONDS_TO_START_FIGHT;

        /*
        //Get current players and calculates seconds to respawn
        secondsToRespawnShells =  (((float)GameState.numberOfPlayers * multiplierToRespawn) / 100f) * Random.Range(0.9f, 1.2f);
        secondsToRespawnShells = secondsToRespawnShells + GameState.numberOfPlayers;
        Debug.Log($"secondsToRespawnShells: {secondsToRespawnShells}");

        //calculate first shells
        initialShells = GameState.numberOfPlayers + 10; //TODO: colocar alguma codicao de tempo MAX
        decayOfShells = GameState.numberOfPlayers - Random.Range(1 / GameState.numberOfPlayers, 1);

        currentSheels = initialShells;*/

        if (GameState.numberOfPlayers == 2)
        {
            PlacarPlayer3.SetActive(false);
            PlacarPlayer4.SetActive(false);
        }

        if (GameState.numberOfPlayers == 3)
        {
            PlacarPlayer4.SetActive(false);
        }

        if (shellPrefab == null)
        {
            Debug.LogError("Shell prefab not set!!!");
        }

        //instantiateShells();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameState.IsFightStarted && GameState.IsFinished == false && GameState.IsPlaying)
        {
            timeRemainingToSpawn -= Time.deltaTime;
            if (timeRemainingToSpawn < 0.0f) {
                var maxShells = Mathf.Min( GameState.numberOfPlayers - 1, 2.0f);
                //if (maxShells >= 2)
                //{
                //    if (GameState.RemainingTimeRatio < 0.5f)
                //    {
                //        maxShells--;
                //    }
                //}
                var minShells = 1 + (maxShells - 1) * GameState.RemainingTimeRatio;

                var numberOfShells = Random.Range(minShells, maxShells);
                instantiateShells(numberOfShells);

                defineNewTimeToSpawn();
            }
        }

        /* elapsedTimeToRespawnShells += Time.deltaTime;
        if(elapsedTimeToRespawnShells > secondsToRespawnShells)
        {
            var randShellsToSpawn = Random.Range(1 / GameState.numberOfPlayers, 2);
            instantiateShells(randShellsToSpawn);

            currentSheels -= decayOfShells;
            elapsedTimeToRespawnShells = 0.0f;
            //Debug.Log($"decayOfShells: {decayOfShells} and currentSheels: {currentSheels}");
        } */
    }

    private void instantiateShells(float shellsToSpawn)
    {
        //Debug.Log($"currentSheels: {shellsToSpawn}");

        //if 0 or less add at least 1
        if (shellsToSpawn < 1)
            shellsToSpawn = 1;

        //never more than 3?
        if (shellsToSpawn >= 3)
            shellsToSpawn = 3;

        for (int i = 0; i < Mathf.RoundToInt(shellsToSpawn); i++)
        {
            if(shellsCount >= Mathf.RoundToInt(GameState.numberOfPlayers / 1.9f) )
            {
                return;
            }

            if (shellPrefab != null)
            {
                //Debug.Log("Far: " + maxFar);
                //Debug.Log("Near: " + minNear);

                Vector3 screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width), Random.Range(Screen.height * minY, Screen.height * maxY), Random.Range(minNear, maxFar)));
                Instantiate(shellPrefab, screenPosition, Quaternion.identity);
                shellsCount++;
            }
        }
    }
}
