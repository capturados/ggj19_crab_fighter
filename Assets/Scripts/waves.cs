﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waves : MonoBehaviour
{
    public float frequency = 0.5f;
    public float amplitudeZ = 0.5f;
    public float amplitudeX = 0.5f;

    Transform _transform;

    private float zPosition;
    private float xPosition;

    void Awake()
    {

    }

    void Start()
    {
        _transform = GetComponent<Transform>();
        zPosition = _transform.position.z;
        xPosition = _transform.position.x;
    }

    void Update()
    {
        // Update the up/down position.
        //zPosition += increaseZPosition * Time.deltaTime;
        //if (zPosition > Mathf.PI)
        //{
        //    zPosition = zPosition - Mathf.PI;
        //}
        var pos = _transform.position;
        _transform.position = new Vector3((Mathf.Sin(Time.time) * amplitudeX) + xPosition, pos.y, (Mathf.Sin(Time.time) * amplitudeZ) + zPosition);
    }
}
